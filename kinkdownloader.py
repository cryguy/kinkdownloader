#!/usr/bin/python3
##############################################################################
# kinkdownloader, v0.4.2 - Downloads Kink.com videos and metadata.
#
# Copyright (C) 2020 MeanMrMustardGas <meanmrmustardgas at protonmail dot com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##############################################################################
import argparse
import datetime
import os
import signal
import sys
from http.cookiejar import MozillaCookieJar
from multiprocessing import Pool
from pathlib import Path
from xml.sax.saxutils import escape

import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

# Set up command line arguments
parser = argparse.ArgumentParser(description=''' Download shoot
                                                 videos from kink.com''')
parser.add_argument("url", help="Shoot or channel gallery URL.")
parser.add_argument("-q", "--quality", help="Select video quality.",
                    choices=["1080", "720", "540", "480", "360",
                             "288", "270"])
parser.add_argument("-c", "--cookies", metavar="cookies.txt",
                    help="Location of Netscape cookies.txt file")
parser.add_argument("--no-video", action="store_true", default=False,
                    help="Don't download shoot video[s].")
parser.add_argument("-m", "--no-metadata", action="store_true", default=False,
                    help="Don't download any additional metadata,")
parser.add_argument("-n", "--no-nfo", action="store_true", default=False,
                    help="Don't create emby-compatible nfo file.")
parser.add_argument("-b", "--no-bio", action="store_true", default=False,
                    help="Don't download actor thumbnails.")
parser.add_argument("-t", "--no-thumbs", action="store_true", default=False,
                    help="Don't download shoot thumbnails.")
parser.add_argument("-p", "--no-poster", action="store_true", default=False,
                    help="Don't download shoot poster image.")
parser.add_argument("--bio-dir", default=".", metavar="dir",
                    help="Thumbnail base directory.")
parser.add_argument("-d", "--debug", action="store_true", default=False,
                    help="Write debugging information to 'debug.log'")
args = parser.parse_args()


def get_html(url, cookie):
    """
    Gets html for processing.
    """
    req = requests.get(url, cookies=cookie)
    html = req.text
    soup = BeautifulSoup(html, "html.parser")

    return soup


def get_dl_url(html, quality):
    """
    Gets download URL for chosen or nearest lower available quality.
    """
    quality_list = ["1080", "720", "540", "480", "360", "288", "270"]
    index = quality_list.index(quality)
    downloads = html.find_all("a", download=True)
    while index < len(quality_list):
        for tag in downloads:
            if quality_list[index] in tag.get_text():
                dl_url = tag.attrs['download']
                return dl_url
        index += 1
    return None


def get_filename(url):
    """
    Get filename from url.
    """
    f_name = url.split("/")[-1]
    f_name = f_name.split("?")[0]
    return f_name


def get_metadata(src):
    """
    Parse src for shoot metadata.
    """
    title = src.find("h1", "shoot-title").get_text()

    desc = src.find("span", "description-text")
    desc = desc.find("p")
    if desc is None:
        desc = "No Description Available"
    else:
        desc = desc.get_text()

    if src.find("span", "names h5") is None:
        actors = None
    else:
        actors = src.find("span", "names h5")
        actors = actors.get_text().strip().split(",")

    shoot_date = src.find("span", "shoot-date")
    shoot_date = datetime.datetime.strptime(shoot_date.get_text(), '%B %d, %Y')
    shoot_date = shoot_date.strftime("%Y-%m-%d")

    genres = []
    actor_thumbs = []

    for name in src.find_all("span", "names h5"):
        for bio in name.find_all("a"):
            bio_url = "https://www.kink.com" + bio.attrs['href']
            bio_page = BeautifulSoup(requests.get(bio_url).text, "html.parser")

            if bio_page.find("img", "bio-slider-img") is None:
                if bio_page.find("img", "bio-img") is None:
                    img = "https://cdnp.kink.com/imagedb/43869/i/h/410/16.jpg"
                else:
                    img = bio_page.find("img", "bio-img").attrs['src']
            else:
                img = bio_page.find("img", "bio-slider-img").attrs['src']
            actor_thumbs.append(img)

    for tag in src.find_all("a", "tag"):
        g = tag.get_text().replace(',', '').strip()
        genres.append(g)

    metadata = {
        "title": title,
        "description": desc,
        "releasedate": shoot_date,
        "genres": genres,
        "actors": actors,
        "actor_thumbs": actor_thumbs
    }
    return metadata


def get_thumb_url(html):
    """
    Parse html for thumbnail zip url
    """
    tags = html.findAll("a", "zip-links")
    try:
        thumb_urls = [tag['href'] for tag in tags]
    except TypeError:
        print("Thumbnail URL not found. Skipping.")
        return None
    return thumb_urls


def get_poster_url(html):
    tag = html.find("video", {"id": "kink-player"})
    try:
        poster_url = tag['poster']
    except TypeError:
        print("Poster URL not found. Skipping.")
        return None
    return poster_url


def download_file(url, f_name, cookie_file):
    """
    Download url and save as filename with progressbar
    """
    cookie = MozillaCookieJar(cookie_file)
    cookie.load(ignore_expires=True, ignore_discard=True)

    f_path = Path(f_name)

    if Path.exists(f_path):
        r = requests.head(url, cookies=cookie)
        length = r.headers['Content-Length']
        f_size = Path(f_name).stat().st_size
        if not int(length) > f_size:
            print("File " + f_name + " exists: Skipping.")
            return 1

    chunk_size = 1024
    dl = requests.get(url, cookies=cookie, stream=True, allow_redirects=True)
    with open(f_name, "wb") as f_out:
        with tqdm(unit="B", unit_scale=True, unit_divisor=1024, miniters=1,
                  desc=f_name, total=int(dl.headers.get('content-length'))
                  ) as pbar:
            for chunk in dl.iter_content(chunk_size=chunk_size):
                f_out.write(chunk)
                pbar.update(len(chunk))


def write_metadata_nfo(metadata, f_name):
    """
    Write metadata to emby compatible NFO file.
    """
    f_name = f_name.split(".")[0] + ".nfo"
    nfo = open(f_name, "w")
    nfo.write('<?xml version="1.0" encoding="utf-8" standalone="yes"?>' + "\n")
    nfo.write("<movie>" + "\n")
    nfo.write(
        "  <plot>" + escape(metadata['description']).encode("ascii", "ignore").decode("ascii").strip() + "</plot>\n")
    nfo.write("  <title>" + escape(metadata['title']).encode("ascii", "ignore").decode("ascii").strip() + "</title>\n")
    nfo.write("  <releasedate>" + escape(metadata['releasedate']).encode("ascii", "ignore").decode("ascii").strip() +
              "</releasedate>\n")
    for tag in metadata['genres']:
        nfo.write("  <genre>" + escape(tag.strip()).encode("ascii", "ignore").decode("ascii").strip() + "</genre>\n")
    nfo.write("  <studio>Kink.com</studio>\n")
    if metadata['actors'] is not None:
        for actor, actor_thumb in zip(metadata['actors'], metadata['actor_thumbs']):
            nfo.write("  <actor>\n")
            nfo.write(
                "    <name>" + escape(actor.strip()).encode("ascii", "ignore").decode("ascii").strip() + "</name>\n")
            nfo.write("    <type>Actor</type>\n")
            nfo.write("    <thumb>" + escape(actor_thumb) +
                      "</thumb>\n")
            nfo.write("  </actor>\n")
    nfo.write("</movie>\n")
    nfo.close()


def dl_actor_thumb(metadata, path):
    """
    Download actor thumbnail
    """
    if metadata['actors'] is None:
        print("No performers listed for shoot.\n")
        return False
    for actor, actor_thumb in zip(metadata['actors'], metadata['actor_thumbs']):
        actor = actor.strip()
        dl_path = path + "/" + actor[0].upper() + "/" + actor
        if not os.path.isdir(dl_path):
            try:
                os.makedirs(dl_path)
            except OSError:
                print("Creation of directory %s failed." % dl_path)
        if not os.path.isfile(dl_path + "/poster.jpg"):
            dl = requests.get(actor_thumb)
            with open(dl_path + "/poster.jpg", "wb") as f_out:
                for chunk in dl.iter_content(1024):
                    f_out.write(chunk)
    return True


def func_wrapper(_args):
    globals()[_args[0]](*_args[1])


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def process_shoot(url, quality, cookie_file, _continue):
    cookie = MozillaCookieJar(cookie_file)
    cookie.load(ignore_expires=True, ignore_discard=True)
    soup = get_html(url, cookie)
    dl_url = get_dl_url(soup, quality)
    if dl_url is None:
        if not _continue:
            sys.exit("Download unavailable for url: " + url + "\n")
        return
    f_name = get_filename(dl_url)
    task = []
    args_list = []
    if args.no_video is False:
        task.append("download_file")
        args_list.append([dl_url, f_name, cookie_file])
    if args.no_nfo is False:
        task.append("write_metadata_nfo")
        args_list.append([get_metadata(soup), f_name])
    if args.no_bio is False:
        task.append("dl_actor_thumb")
        args_list.append([get_metadata(soup), os.path.expanduser(args.bio_dir)])
    if args.no_thumbs is False:
        thumb_urls = get_thumb_url(soup)
        if thumb_urls is not None:
            for thumb_url in thumb_urls:
                task.append("download_file")
                args_list.append([thumb_url, get_filename(thumb_url), cookie_file])
    if args.no_poster is False:
        poster_url = get_poster_url(soup)
        if poster_url is not None:
            p_name = f_name.split(".")[0] + "." + poster_url.split(".")[-1]
            task.append("download_file")
            args_list.append([poster_url, p_name, cookie_file])
    p = Pool(4, init_worker)
    try:
        p.daemon = True
        list(p.imap(func_wrapper, zip(task, args_list)))
        p.close()
        p.join()
    except KeyboardInterrupt:
        p.terminate()
        p.join()
        sys.exit("Keyboard interrupt received, exiting")


def get_shoots_from_list_page(url, cookie):
    soup = get_html(url, cookie)
    shoots = soup.find_all("a", "shoot-link")
    shoot_urls = []
    for shoot_id in shoots:
        shoot_urls.append("https://www.kink.com" + shoot_id['href'])
    return shoot_urls


def main():
    # Grab cookies from netscape cookie format file, and create cookie jar.
    if args.cookies is None:
        cookie_file = os.path.expanduser("~/cookies.txt")
    else:
        cookie_file = os.path.expanduser(args.cookies)

    cookie = MozillaCookieJar(cookie_file)
    cookie.load(ignore_expires=True, ignore_discard=True)

    if args.quality is None:
        quality = "1080"
    else:
        quality = args.quality

    if args.no_metadata is True:
        args.no_bio = True
        args.no_thumbs = True
        args.no_nfo = True
        args.no_poster = True

    # Grab shoot url from commandline arguments
    url = args.url
    print("\n" + "Scraping from " + url)
    if url.split('/')[3] == "shoot":
        process_shoot(url, quality, cookie_file, False)

    elif url.split('/')[3] == "channel":
        shoot_urls = get_shoots_from_list_page(url, cookie)
        for download in shoot_urls:
            process_shoot(download, quality, cookie_file, True)
    else:
        print("Error: Invalid URL.")


if __name__ == '__main__':
    main()
