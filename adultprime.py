import argparse
import os
import signal
import sys
from http.cookiejar import MozillaCookieJar
from multiprocessing import Pool
from pathlib import Path
from xml.sax.saxutils import escape
from urllib.parse import urljoin
import requests
from bs4 import BeautifulSoup
from tqdm import tqdm

# Set up command line arguments
parser = argparse.ArgumentParser(description=''' Download shoot
                                                 videos from adultprime.com''')
parser.add_argument("url", help="Shoot or channel gallery URL.")
parser.add_argument("-q", "--quality", help="Select video quality.",
                    choices=["4k", "1080p", "720p", "540p"])
parser.add_argument("-c", "--cookies", metavar="cookies.txt",
                    help="Location of Netscape cookies.txt file")
parser.add_argument("--no-video", action="store_true", default=False,
                    help="Don't download shoot video[s].")
parser.add_argument("-m", "--no-metadata", action="store_true", default=False,
                    help="Don't download any additional metadata,")
parser.add_argument("-n", "--no-nfo", action="store_true", default=False,
                    help="Don't create emby-compatible nfo file.")
parser.add_argument("-b", "--no-bio", action="store_true", default=False,
                    help="Don't download actor thumbnails.")
parser.add_argument("-t", "--no-thumbs", action="store_true", default=False,
                    help="Don't download shoot thumbnails.")
parser.add_argument("-p", "--no-poster", action="store_true", default=False,
                    help="Don't download shoot poster image.")
parser.add_argument("--bio-dir", default=".", metavar="dir",
                    help="Thumbnail base directory.")
parser.add_argument("-d", "--debug", action="store_true", default=False,
                    help="Write debugging information to 'debug.log'")
args = parser.parse_args()


def get_html(url, cookie):
    """
    Gets html for processing.
    """
    req = requests.get(url, headers={
        'authority': 'adultprime.com',
        'pragma': 'no-cache',
        'cache-control': 'no-cache',
        'sec-ch-ua': '"Chromium";v="92", " Not A;Brand";v="99", "Google Chrome";v="92"',
        'sec-ch-ua-mobile': '?0',
        'upgrade-insecure-requests': '1',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/92.0.4515.131 Safari/537.36',
        'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,'
                  'application/signed-exchange;v=b3;q=0.9',
        'service-worker-navigation-preload': 'true',
        'sec-fetch-site': 'none',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-user': '?1',
        'sec-fetch-dest': 'document',
        'accept-language': 'en-US,en;q=0.9'
    }, cookies=cookie)
    html = req.text
    soup = BeautifulSoup(html, "html.parser")

    return soup


def get_dl_url(html, quality):
    """
    Gets download URL for chosen or nearest lower available quality.
    """
    quality_list = ["4k", "1080p", "720p", "540p"]
    index = quality_list.index(quality)
    downloads = html.find_all("a", "stream-quality-selection download-link")
    while index < len(quality_list):
        for tag in downloads:
            if quality_list[index] in tag.get_text():
                dl_url = tag.attrs['href']
                return dl_url
        index += 1
    return None


def get_metadata(src):
    """
    Parse src for shoot metadata.
    """
    title = src.find("h2", "update-info-title").get_text()

    desc = src.findAll("p", "update-info-line")[-1]
    if desc is None:
        desc = "No Description Available"
    else:
        desc = desc.get_text()

    genres = []

    for tag in src.find_all("a", "site-link")[2:-3]:
        g = tag.get_text().replace(',', '').strip()
        genres.append(g)

    metadata = {
        "title": title,
        "description": desc,
        "genres": genres,
    }
    return metadata


def get_thumb_url(soup, cookie_file):
    """
    Parse html for thumbnail zip url
    """
    cookie = MozillaCookieJar(cookie_file)
    cookie.load(ignore_expires=True, ignore_discard=True)
    try:
        url = urljoin("https://adultprime.com/studios/video/",
                      soup.find("a", "btn btn-block link-item")['href'])
        html = get_html(url, cookie)
        tags = html.find("a", "photoset-action-link site-link")
        thumb_urls = tags['href']
    except:
        print("Thumbnail URL not found. Skipping.")
        return None
    return thumb_urls


def get_poster_url(html):
    tag = html.find("video")
    try:
        poster_url = tag['poster']
    except TypeError:
        print("Poster URL not found. Skipping.")
        return None
    return poster_url


def download_file(url, f_name, cookie_file):
    """
    Download url and save as filename with progressbar
    """
    cookie = MozillaCookieJar(cookie_file)
    cookie.load(ignore_expires=True, ignore_discard=True)

    f_path = Path(f_name)

    if Path.exists(f_path):
        r = requests.head(url, cookies=cookie)
        length = r.headers['Content-Length']
        f_size = Path(f_name).stat().st_size
        if not int(length) > f_size:
            print("File " + f_name + " exists: Skipping.")
            return 1

    chunk_size = 1024
    dl = requests.get(url, cookies=cookie, stream=True, allow_redirects=True)
    with open(f_name, "wb") as f_out:
        with tqdm(unit="B", unit_scale=True, unit_divisor=1024, miniters=1,
                  desc=f_name, total=int(dl.headers.get('content-length'))
                  ) as pbar:
            for chunk in dl.iter_content(chunk_size=chunk_size):
                f_out.write(chunk)
                pbar.update(len(chunk))


def write_metadata_nfo(metadata, f_name):
    """
    Write metadata to emby compatible NFO file.
    """
    f_name = f_name.split(".")[0] + ".nfo"
    nfo = open(f_name, "w")
    nfo.write('<?xml version="1.0" encoding="utf-8" standalone="yes"?>' + "\n")
    nfo.write("<movie>" + "\n")
    nfo.write(
        "  <plot>" + escape(metadata['description']).encode("ascii", "ignore").decode("ascii").strip() + "</plot>\n")
    nfo.write("  <title>" + escape(metadata['title']).encode("ascii", "ignore").decode("ascii").strip() + "</title>\n")
    nfo.write("</movie>\n")
    nfo.close()


def func_wrapper(_args):
    globals()[_args[0]](*_args[1])


def init_worker():
    signal.signal(signal.SIGINT, signal.SIG_IGN)


def process_shoot(shoot_id, quality, cookie_file, _continue):
    p = Pool(4, init_worker)
    try:
        url = "https://adultprime.com/studios/video/" + str(shoot_id)
        cookie = MozillaCookieJar(cookie_file)
        cookie.load(ignore_expires=True, ignore_discard=True)
        soup = get_html(url, cookie)
        dl_url = get_dl_url(soup, quality)
        if dl_url is None:
            if not _continue:
                sys.exit("Download unavailable for url: " + url + "\n")
            return
        metadata = get_metadata(soup)
        f_name = metadata['title']
        task = []
        args_list = []
        if args.no_video is False:
            task.append("download_file")
            args_list.append([dl_url, f_name + ".mp4", cookie_file])
        if args.no_nfo is False:
            task.append("write_metadata_nfo")
            args_list.append([get_metadata(soup), f_name])
        if args.no_thumbs is False:
            thumb_url = get_thumb_url(soup, cookie_file)
            if thumb_url is not None:
                task.append("download_file")
                args_list.append([thumb_url, f_name + ".zip", cookie_file])
        if args.no_poster is False:
            poster_url = get_poster_url(soup)
            if poster_url is not None:
                p_name = f_name.split(".")[0] + "." + poster_url.split(".")[-1]
                task.append("download_file")
                args_list.append([poster_url, p_name, cookie_file])
        p.daemon = True
        list(p.imap(func_wrapper, zip(task, args_list)))
        p.close()
        p.join()
    except KeyboardInterrupt:
        p.terminate()
        p.join()
        sys.exit("Keyboard interrupt received, exiting")


def get_shoots_from_list_page(url, cookie):
    soup = get_html(url, cookie)
    shoots = soup.find_all("div", "col-xs-12 col-xs-6-landscape col-sm-6 col-sm-4 col-md-3 mt-10")

    shoot_urls = []
    for shoot_id in shoots:
        div = shoot_id.find("div", "overlay inline-preview")
        shoot_urls.append(div['data-id'])
    return shoot_urls


def main():
    # Grab cookies from netscape cookie format file, and create cookie jar.
    if args.cookies is None:
        cookie_file = os.path.expanduser("~/cookies.txt")
    else:
        cookie_file = os.path.expanduser(args.cookies)

    cookie = MozillaCookieJar(cookie_file)
    cookie.load(ignore_expires=True, ignore_discard=True)

    if args.quality is None:
        quality = "1080p"
    else:
        quality = args.quality

    if args.no_metadata is True:
        args.no_bio = True
        args.no_thumbs = True
        args.no_nfo = True
        args.no_poster = True

    # Grab shoot url from commandline arguments
    url = args.url
    print("\n" + "Scraping from " + url)
    if url.split('/')[4] == "video":
        process_shoot(url.split('/')[5], quality, cookie_file, False)

    elif url.split('/')[4][0:6] == "videos":
        shoot_urls = get_shoots_from_list_page(url, cookie)
        for download in shoot_urls:
            process_shoot(download, quality, cookie_file, True)
    else:
        print("Error: Invalid URL.")


if __name__ == '__main__':
    main()
